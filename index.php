<?php

  ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);

  $folder = "portfolio";
  $file = $folder . "/datas.csv";
  $datas = array_map('str_getcsv', file($file));
  $columns = $datas[0];
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?= $folder ?></title>
  <link rel="stylesheet" href="style/main.css">
</head>
<body>
  <main>
    <section class="columns">
      <?php foreach ($columns as $column) : ?>
        <div class="column" data-type="<?= $column ?>"><?= $column ?></div>
      <?php endforeach; ?>
    </section>
    <?php foreach ($datas as $project) : ?>
      <?php if ($project == $datas[0]) continue; ?>
      <section class="project">
        <?php foreach ($project as $content) : ?>
          <div class="column">
            <?php if ($content == $project[1]) : ?>
              <?php
                $path = $folder . '/medias/' . $content;
                $projectFolder = opendir($path);
                while($file = readdir($projectFolder)) {
                  if($file != '.' && $file != '..') {
                    echo '<img src="' . $path. '/' . $file . '">';
                  }
                }
                closedir($projectFolder);
              ?>
            <?php else : ?>
              <?= $content ?>
            <?php endif; ?>
          </div>
        <?php endforeach; ?>
        </section>
    <?php endforeach; ?>
  </main>
</body>
</html>